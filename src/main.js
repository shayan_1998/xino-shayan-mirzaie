import { createApp } from "vue";
import App from "./App.vue";
import "./index.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import clickOutside from "@/directive/clickOutside";

library.add(faPlus);

const app = createApp(App);
app.component("font-awesome-icon", FontAwesomeIcon);
app.directive("click-outside", clickOutside);
app.mount("#app");
